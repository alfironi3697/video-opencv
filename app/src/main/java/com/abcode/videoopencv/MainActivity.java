package com.abcode.videoopencv;

import android.database.Cursor;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameFilter;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.FFmpegLogCallback;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameGrabber;
import org.bytedeco.javacv.OpenCVFrameRecorder;
//import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends FragmentActivity implements VideoSelectorFragment.OnVideoSelectedListener, LoaderManager.LoaderCallbacks<Cursor>,
        View.OnClickListener {

    private static final int VIDEO_CURSOR_LOADER_ID = 10;

    private static final String[] PROJECTION = new String[] { MediaStore.Video.Media._ID, MediaStore.Video.Media.DATA, MediaStore.Video.Media.DATE_ADDED };

    private VideoSelectorFragment vsf;
    private FrameLayout selectorLayout;
    private CoordinatorLayout root;
    private Button selectVideoBtn, processVideoBtn;

    private String videoPath;
    private ProgressBar pb;
    private RelativeLayout pbLayout;

    private VideoView vidView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //OpenCVLoader.initDebug();

        vsf = new VideoSelectorFragment();

        selectorLayout = findViewById(R.id.chooser_fragment_layout);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.chooser_fragment_layout, vsf)
                .commit();

        root = findViewById(R.id.root);

        selectVideoBtn = findViewById(R.id.select_vid_btn);
        processVideoBtn = findViewById(R.id.process_vid_btn);
        selectVideoBtn.setOnClickListener(this);
        processVideoBtn.setOnClickListener(this);

        pb = findViewById(R.id.encoding_progress_bar);
        pbLayout = findViewById(R.id.encoding_process_layout);
        vidView = findViewById(R.id.result_video_preview);

        getSupportLoaderManager().initLoader(VIDEO_CURSOR_LOADER_ID, null, this);
    }

    /*
    private MediaExtractor createExtractor() throws IOException {
        MediaExtractor extractor;
        extractor = new MediaExtractor();
        extractor.setDataSource(this, Uri.fromFile(new File(videoPath)), null);
        //extractor.setDataSource(mContext, Uri.parse(mUriList.get(index)), null);
        return extractor;
    }

    private int getAndSelectVideoTrackIndex(MediaExtractor extractor) {
        for (int index = 0; index < extractor.getTrackCount(); ++index) {
            if (isVideoFormat(extractor.getTrackFormat(index))) {
                extractor.selectTrack(index);
                return index;
            }
        }
        return -1;
    }

    private static boolean isVideoFormat(MediaFormat format) {
        return getMimeTypeFor(format).startsWith("video/");
    }

    private static String getMimeTypeFor(MediaFormat format) {
        return format.getString(MediaFormat.KEY_MIME);
    }
    */

    private void startProcessVideo() {

        int index = 0;
        new AsyncTask<Void, Void, String>() {

            FFmpegFrameGrabber vCap;
            FFmpegFrameRecorder vRec;
            FFmpegFrameFilter vFilter;
            File outFile;
            Exception exception = null;
            int framesLen = 0;
            long start = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                FFmpegLogCallback.set();
                vCap = new FFmpegFrameGrabber(videoPath);
                try {
                    vCap.start();
                } catch (FFmpegFrameGrabber.Exception e) {
                    exception = e;
                }
                outFile = VideoUtils.getOutputMediaFile(Constant.MEDIA_TYPE_VIDEO);

                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(videoPath);
                String rotation = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
                mmr.release();

                int width = vCap.getImageWidth(), height = vCap.getImageHeight();
                int rotWidth, rotHeight;
                int rotInDegs = Integer.valueOf(rotation);
                if(rotInDegs % 180 == 90) {
                    rotWidth = height;
                    rotHeight = width;
                } else {
                    rotWidth = width;
                    rotHeight = height;
                }

                vRec = new FFmpegFrameRecorder(outFile+"", rotWidth, rotHeight, vCap.getAudioChannels());
                try {
                    vRec.start();
                } catch (FFmpegFrameRecorder.Exception e){
                    exception = e;
                }
                vRec.setFormat("mp4");
                vRec.setFormat(vCap.getFormat());
                vRec.setVideoCodec(avcodec.AV_CODEC_ID_MPEG4);
                vRec.setVideoBitrate(vCap.getVideoBitrate());
                vRec.setAudioCodec(vCap.getAudioCodec());
                vRec.setAudioBitrate(vCap.getAudioBitrate());
                vRec.setAudioChannels(vCap.getAudioChannels());
                double fr = vCap.getFrameRate();
                Log.i("ffmpeg_frame_rate", fr+" "+vCap.getFormat()+" "+vCap.getVideoCodec());
                vRec.setFrameRate(fr);

                double rotInRads = Math.toRadians(rotInDegs);
                vFilter = new FFmpegFrameFilter("mpdecimate, "+(rotInDegs == 90 ? "transpose=clock," : (rotInDegs == 270 ? "transpose=cclock," :""))+" colorchannelmixer=.3:.4:.3:0:.3:.4:.3:0:.3:.4:.3, colorlevels=rimin=0.039:gimin=0.039:bimin=0.039:rimax=0.96:gimax=0.96:bimax=0.96, colorlevels=romin=0.3:gomin=0.3:bomin=0.3", vCap.getImageWidth(), vCap.getImageHeight());
                //vFilter = new FFmpegFrameFilter("colorchannelmixer=.3:.4:.3:0:.3:.4:.3:0:.3:.4:.3, colorlevels=rimin=0.039:gimin=0.039:bimin=0.039:rimax=0.96:gimax=0.96:bimax=0.96, colorlevels=romin=0.3:gomin=0.3:bomin=0.3", vCap.getImageWidth(), vCap.getImageHeight());
                vFilter.setPixelFormat(vCap.getPixelFormat());
                try {
                    vFilter.start();
                } catch (FFmpegFrameFilter.Exception e) {
                    exception = e;
                }

                pbLayout.setVisibility(View.VISIBLE);
                start = System.currentTimeMillis();
            }

            @Override
            protected String doInBackground(Void... voids) {

                framesLen = vCap.getLengthInAudioFrames();
                try {
                    Frame f;
                    int i = 0;
                    while((f = vCap.grab()) != null) {
                        if (f.image != null) {
                            vFilter.push(f, vCap.getPixelFormat());
                            Frame filtered;
                            while((filtered = vFilter.pull()) != null) {
                                long timestamp = f.timestamp;
                                Log.i("ffmpeg_recorder", "processing frame " + i + " " + timestamp+" "+vCap.getLengthInVideoFrames());
                                vRec.setTimestamp(timestamp);
                                vRec.record(filtered);
                            }
                            //vRec.record(f);
                        } else {
                            long timestamp = f.timestamp;
                            Log.i("ffmpeg_recorder", "frame " + i + " image is null, sound only " + timestamp+" "+vCap.getLengthInAudioFrames());
                            vRec.setTimestamp(timestamp);
                            vRec.record(f);
                        }
                        i++;
                    }
                    Log.i("compare_frame_nums", i+" "+framesLen);
                } catch (FFmpegFrameGrabber.Exception e) {
                    exception = e;
                } catch (FFmpegFrameRecorder.Exception e) {
                    exception = e;
                } catch (FFmpegFrameFilter.Exception e) {
                    exception = e;
                }

                try {
                    vCap.flush();
                    vCap.release();
                    vRec.stop();
                    vRec.release();
                    vFilter.stop();
                    vFilter.release();
                } catch (FFmpegFrameGrabber.Exception e) {
                    exception = e;
                } catch (FFmpegFrameRecorder.Exception e) {
                    exception = e;
                } catch (FFmpegFrameFilter.Exception e) {
                    exception = e;
                }

                if (exception != null) {
                    Log.e("ffmpeg_exception", exception.getMessage()+"");
                    return null;
                } else {
                    return outFile+"";
                }
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                pbLayout.setVisibility(View.GONE);
                vidView.setVideoPath(s);
                vidView.start();

                long duration = (System.currentTimeMillis() - start);
                final Snackbar snackbar = Snackbar.make(root, "Encoding finished in "+duration+" ms", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                });
                snackbar.show();
            }
        }.execute();
    }

    @Override
    public void onVideoSelected(String videoPath) {
        Log.i("video_path", videoPath);
        this.videoPath = videoPath;
        selectorLayout.setVisibility(View.GONE);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        return new CursorLoader(this, MediaStore.Video.Media.EXTERNAL_CONTENT_URI, PROJECTION, null, null, MediaStore.Video.Media.DATE_ADDED+" DESC" );
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        Log.i("cursor_count", cursor.getCount()+"");
        vsf.onCursorFinished(cursor);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    @Override
    public void onClick(View view) {
        if(view == selectVideoBtn) {
            selectorLayout.setVisibility(View.VISIBLE);
        } else if(view == processVideoBtn) {
            startProcessVideo();
        }
    }
}
