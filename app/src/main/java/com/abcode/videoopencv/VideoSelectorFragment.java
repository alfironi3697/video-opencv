package com.abcode.videoopencv;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VideoSelectorFragment.OnVideoSelectedListener} interface
 * to handle interaction events.
 * Use the {@link VideoSelectorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VideoSelectorFragment extends Fragment {

    private OnVideoSelectedListener mListener;

    private RecyclerView selectorRV;
    private VideoAdapter vAdapter;

    public VideoSelectorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment VideoSelectorFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VideoSelectorFragment newInstance() {
        VideoSelectorFragment fragment = new VideoSelectorFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_video_selector, container, false);
        selectorRV = v.findViewById(R.id.video_select_rv);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectorRV.setLayoutManager(new GridLayoutManager(getActivity(), 3));
    }

    public void onCursorFinished(Cursor c) {
        vAdapter = new VideoAdapter(getActivity(), c, mListener);
        selectorRV.setAdapter(vAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnVideoSelectedListener) {
            mListener = (OnVideoSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnVideoSelectedListener {
        // TODO: Update argument type and name
        void onVideoSelected(String videoPath);
    }
}
