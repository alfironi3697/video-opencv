LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := app
LOCAL_LDFLAGS := -Wl,--build-id
LOCAL_SRC_FILES := \
	D:\video-opencv\app\src\main\jni\armeabi-v7a\libopencv_java3.so \

LOCAL_C_INCLUDES += D:\video-opencv\app\src\main\jni
LOCAL_C_INCLUDES += D:\video-opencv\app\src\debug\jni

include $(BUILD_SHARED_LIBRARY)
